class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.string :rcv_name
      t.text :rcv_address
      t.string :rcv_contact_number
      t.string :invoise_number
      t.string :status, default: 'active'

      t.timestamps
    end
  end
end
