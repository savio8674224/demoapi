class Order < ApplicationRecord

  has_many :order_books
  has_many :books, through: :order_books
  validates :status, inclusion: { in: %w( active cancel),
    message: "%{value} is not a valid status" }

  accepts_nested_attributes_for :order_books
end
