class Api::V1::OrdersController < ApplicationController
  def index
    orders = Order.where(type_search).paginate(page: params[:page], per_page: params[:per_page])
    render json: orders
  end

  def create
    order = Order.new(order_create_params)
    if order.save
      render json: order
    else
      render json: {"error": "Order not successfull"}
    end
  end

  def show
    order = Order.find_by(id:params[:id])
    render json: order
  end

  def destroy
    order = Order.find_by(id:params[:id])
    order.status = 'cancel'
    if order.save!
      render json: order
    else
      render json: {"error": "Order is not cancel"}
    end
  end
  private

  def order_create_params
    book = Book.find(  params[:orders][:book_id].to_i)
    search_params = {rcv_name: params[:orders][:rcv_name], rcv_address: params[:orders][:rcv_address], 
      rcv_contact_number: params[:orders][:rcv_contact_number],
      invoise_number: params[:orders][:invoise_number],
      status: params[:orders][:status],
      books: [book]}
  end

  def type_search
    return {} unless params[:type].present?
     
     {status: params[:type]}
  end

end
