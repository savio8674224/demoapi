class Api::V1::BooksController < ApplicationController

  def index
    books = Book.where(search_params_attr).order(sort_condition).paginate(page: params[:page], per_page: params[:per_page])
    render json: books
  end

  def show
    book = Book.find_by(id:params[:id])
    if book
      render json: book
    else
      render json: {error: "Book Not Found"}
    end
  end

  def create
    book = Book.new(
      name: book_params[:name],
      price: book_params[:price],
      author: book_params[:author],
      year: book_params[:year]
      )
    if book.save
      render json: book
    else
      render json: {error: "Book not create"}
    end
  end

  def update
    book = Book.find_by(id:params[:id])
    if book
      book.update(available_copies: params[:book][:available_copies])
      render json: book
    else
      render json: {error: "Book not create"}
    end
  end 

  def destroy
    book = Book.find_by(id:params[:id])
    if book
      book.update(is_deleted: 1)
      render json: book
    else
      render json: {error: "Book not found"}
    end
  end

  def order_history
    order_history = Book.includes(:orders).where(id: params[:id])
    book_orders = order_history.map(&:orders)
    if book_orders
      render json: book_orders
    else
      render json: {error: "Not Places any Order yet"}
    end
  end

  private

  def book_params
    params.require(:book).permit([:name, :price, :author, :year])
  end

  def sort_condition
    return "name asc" unless ['name', 'year', 'author', 'year'].include? params[:sort]

    sort_column = params[:sort].present? ? params[:sort] : 'name'
    sort_direction = params[:direction].present? ? params[:direction] : 'asc'
    "#{sort_column} #{sort_direction}"
  end

  def search_params_attr
    return {} unless params[:book].present?

    allowed_params = {
      name: params[:book][:name],
      author: params[:book][:author]
    }
    allowed_params.select {|_, val| val.present?}
  end

end
