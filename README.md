# README

# README

1) create, update, delete, view, list books. 
   ========================================
   create :- http://localhost:3000/api/v1/books (post)
   update :- http://localhost:3000/api/v1/books/1 (put)
   delete :- http://localhost:3000/api/v1/books/1 (delete)
   view :- http://localhost:3000/api/v1/books/1 (get)
   list :- http://localhost:3000/api/v1/books (get)

2) update the number of copies available for a book.
   ================================================
   update :- http://localhost:3000/api/v1/books/1 (put)
             book[available_copies]

3) select a book and order it.
   ===========================
   create :- http://localhost:3000/api/v1/orders (post)
             orders[book_id] and all order related values

4) view / cancel an order based on order ID 
   ========================================
   cancel :- http://localhost:3000/api/v1/orders/1 (delete)
   view :- http://localhost:3000/api/v1/orders/1 (get)

5) order history for a specific book. 
   ==================================
   http://localhost:3000/api/v1/books/1/order_history (get)

6) sort the book based on Name, Price, Author, Year Published
   ==========================================================
   list :- http://localhost:3000/api/v1/books (get)
           sort, ditection are params

7)  search books based on Name and Author.
    ======================================
   list :- http://localhost:3000/api/v1/books (get)
           book[name], book[author]

8) active/cancelled orders in the system
   =====================================
   list :- http://localhost:3000/api/v1/orders (get)
           type input

9) paginate the results 
   ====================
   list :- http://localhost:3000/api/v1/orders (get)
           *paginate gem

